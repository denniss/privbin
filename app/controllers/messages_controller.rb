class MessagesController < ApplicationController
  def new
  end

  def create
    # { attachments: [ "yiPNpUPs2fERp1fALzA1YE35CZl6z.." ] }
    @attachment = nil

    if params[:attachments].present? && params[:attachments][0].present?
      @attachment = Attachment.new(content: params[:attachments][0])

      unless @attachment.valid?
        render(json: {error_message: 'Something went wrong!'}, status: :bad_request) and return
      end
    end

    external_id = SecureRandom.hex
    hashed_external_id = Digest::SHA256.hexdigest(external_id)

    while Message.exists?(external_id: hashed_external_id)
      external_id = SecureRandom.hex
      hashed_external_id = Digest::SHA256.hexdigest(external_id)
    end

    @message = Message.new(content: params[:message][:content], external_id: hashed_external_id) #External ID that is

    if @message.save

      if @attachment.present?
        @attachment.message_id = @message.id
        @attachment.save!
      end

      render json: {external_id: external_id}, status: :ok
    else
      render json: {error_message: 'Something went wrong!'}, status: :bad_request
    end
  end

  def show
    hashed_external_id = Digest::SHA256.hexdigest(params[:id]) # id is external_id
    @message = Message.includes(:attachments).find_by_external_id(hashed_external_id)

    if @message.nil?
      render(nothing: true, status: :ok)
    else
      message_content = @message.content
      attachments_arr = @message.attachments.map(&:content)

      @message.destroy

      render json: {message: message_content, attachments: attachments_arr}, status: :ok
    end
  end

  def read
  end

  def destroy
  end

  private
  def message_params
    params.permit(:message).permit(:content)
  end

  def attachments_params
    params.permit(attachments: [])
  end
end
