var Message = (function () {

  var store = {};

  var initCreate = function () {
    store.attachments = [];
    //store.fileLim = 512000; //250 Kilobytes
    //store.fileLimDisplay = '500 KB';

    store.fileLim = 256000; //250 Kilobytes
    store.fileLimDisplay = '250 KB';

    store.createMessageButton = $('#create-message');
    store.imageFileField = $('#image-file-field');
    store.imageUploadButton = $('#image-upload-button');
    store.messageTextArea = $('#message-text-area');
    store.alertBox = $('#alert-box');
    store.alertMessage = $('#alert-message');

    store.imageUploadButton.click(function (e) {
      e.preventDefault();
      store.imageFileField.click();
    });

    store.createMessageButton.attr('disabled', true);
    store.createMessageButton.click(createMessageHandler);
    store.imageFileField.change(imageUploaded);
    store.messageTextArea.bind('input propertychange', messageTextAreaChanged);
  };

  var enableCreateMessageButton = function () {
    store.createMessageButton.attr('disabled', false);
  };

  var disableCreateMessageButton = function () {
    store.createMessageButton.attr('disabled', true);
  };

  var enableCreateMessageButtonIfPossible = function(){
    if(store.messageTextArea.val().length > 0){
      enableCreateMessageButton();
    }else{
      disableCreateMessageButton();
    }
  };

  var imageUploaded = function (e) {
    disableCreateMessageButton();
    store.createMessageButton.attr('disabled', true);
    var file = e.target.files[0];
    var fileType = file.type;
    var fileSize = file.size;

    if (fileSize > store.fileLim) {
      displayAlert("File cannot be greater than " + store.fileLimDisplay);
      enableCreateMessageButtonIfPossible();
      return;
    }

    if (fileType.indexOf("image/") === -1) {
      displayAlert("File must be an image");
      enableCreateMessageButtonIfPossible();
      return;
    }

    var reader = new FileReader();

    reader.onload = function (readerEvt) {
      var binaryString = readerEvt.target.result;
      var fb = btoa(binaryString); //encode data
      var fileSize = fb.length * 6 / 8; //Size bytes

      if (fileSize > store.fileLim) {
        displayAlert("File cannot be greater than " + store.fileLimDisplay);
        enableCreateMessageButtonIfPossible();
      } else {
        store.attachments[0] = fb;
        displayAlert("File is uploaded successfully", true);
        enableCreateMessageButtonIfPossible();
      }
    };

    reader.readAsBinaryString(file);
  };

  var messageTextAreaChanged = function (e) {
    e.preventDefault();
    enableCreateMessageButtonIfPossible();
  };

  var displayAlert = function (message, success) {
    store.alertMessage.html(message);

    if (success) {
      store.alertBox.addClass('success');
      store.alertBox.removeClass('alert');
    } else {
      store.alertBox.addClass('alert');
      store.alertBox.removeClass('success');
    }

    store.alertBox.slideDown();
  };

  var createMessageHandler = function (e) {
    e.preventDefault();

    store.key = CryptoJS.lib.WordArray.random(16).toString();
    store.plaintext = $('#message-text-area').val();
    store.messageCipherText = CryptoJS.AES.encrypt(store.plaintext, store.key).toString();

    if(store.attachments[0]){
      store.attachmentCipherText = CryptoJS.AES.encrypt(store.attachments[0], store.key).toString();
    }

    var token = $('meta[name=csrf-token]').attr('content');

    var doneCb = function (data) {
      var path = "http://privbin.com/r/" + data.external_id + "/#" + store.key;
      $("#msg-url-container").html(path);
      $('#msg-url-modal').foundation('reveal', 'open');
      $('#destroy-button').click(function () {
        window.location.href = path;
      });
    };

    var data = {
      authenticity_token: token,
      message: {
        content: store.messageCipherText
      }
    };

    if(store.attachmentCipherText){
      data.attachments = [store.attachmentCipherText];
    }

    $.post("/messages", data).done(doneCb);
  };

  var initRead = function () {
    var url = document.URL;
    var tokens = url.split('/');
    var extId = tokens[tokens.length - 2];
    var key = window.location.hash;
    var msgContainer = $('#message-container');
    var attachmentContainer = $('#attachment-container');
    var header = $('.messages-read h1');
    var panel = $('.messages-read .panel');

    if (key) {
      key = key.replace("#", "");

      $.get("/messages/" + extId, function (data) {
        if (data) {

          var message = data.message;
          var attachment = data.attachments[0];

          var decryptedMessage = CryptoJS.AES.decrypt(message, key).toString(CryptoJS.enc.Utf8);
          msgContainer.html(decryptedMessage);

          if(attachment){
            var decryptedAttachment = CryptoJS.AES.decrypt(attachment, key).toString(CryptoJS.enc.Utf8);

            var fb = atob(decryptedAttachment); //decoded data

            var byteNumbers = new Array(fb.length);
            for (var i = 0; i < fb.length; i++) {
              byteNumbers[i] = fb.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);

            var imageBlob = new Blob([byteArray]);

            var reader  = new FileReader();

            reader.onloadend = function () {
              var image = new Image();
              image.src = reader.result;
              attachmentContainer.append(image);
            };

            reader.readAsDataURL(imageBlob);
          }

        } else {
          header.html("That message had already been deleted");
          panel.remove();
        }
      });
    } else {
      msgContainer.html("Malformed URL. Message URL must be in the form of http://privbin.com/r/{key1}/#{key2}");
    }
  };

  return {
    initCreate: initCreate,
    initRead: initRead
  };
})();