class Attachment < ActiveRecord::Base
  belongs_to :message

  MAX_CONTENT_LENGTH = 473856 # Approximately 250 KB of UNENCRYPTED length

  validate :attachment_size

  def attachment_size
    self.errors.add(:content, 'is too big') if self.content.length > MAX_CONTENT_LENGTH
  end
end
