class Message < ActiveRecord::Base
  has_many :attachments, dependent: :destroy

  def self.destroy_expired
    messages = Message.where('created_at < ?', 7.days.ago)
    messages.destroy_all
  end
end
