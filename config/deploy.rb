# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'privbin'
set :deploy_user, 'deploy'
set :repo_url, 'git@bitbucket.org:denniss/privbin.git'

set :deploy_to, "/var/apps/#{fetch(:application)}"
set :scm, :git
set :rvm_type, :user
set :rvm_ruby_version, '2.2.0@privbin'
set :use_sudo, '2.2.0'
set :ssh_options, { :forward_agent => true }

set :format, :pretty
set :log_level, :info
set :pty, true
set :port, 8000

set :migration_role, 'postgres'
set :keep_releases, 5
set :environment, 'production'

namespace :deploy do
  before :starting, :stop_passenger

  task :start_passenger do
    on roles(:app), in: :sequence, wait: 5 do
      within "#{fetch(:deploy_to)}/current/" do
        execute :bundle, :exec, "passenger start --daemonize --user #{fetch(:deploy_user)} -e #{fetch(:environment)} -p #{fetch(:port)}"
      end
    end
  end

  task :stop_passenger do
    on roles(:app), in: :sequence, wait: 5 do
      within "#{fetch(:deploy_to)}/current/" do
        execute :bundle, :exec, "passenger stop -p #{fetch(:port)}"
      end
    end
  end

  after :publishing, :start_passenger

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
