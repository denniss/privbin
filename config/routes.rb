Rails.application.routes.draw do
  root 'messages#new'

  get 'how_it_works', to: 'home#how_it_works'
  get 'about', to: 'home#about'

  get 'r/:ext_id', to: 'messages#read'

  resources :messages, only: [:create, :show]
end
