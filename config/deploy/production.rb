role :app, %w{66.175.217.8}
role :web, %w{66.175.217.8}
role :db,  %w{66.175.217.8}

set :rails_env, :production
server '66.175.217.8', user: 'deploy', roles: %w{web app db}