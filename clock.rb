require 'clockwork'
require_relative './config/boot'
require_relative './config/environment'
module Clockwork
  every(1.day, 'Messages.delete_expired', :at => '01:00') {
    Message.destroy_expired;
  }
end