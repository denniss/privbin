class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :external_id, null: false
      t.text :content, null: false
      t.timestamps null: false
    end

    add_index :messages, :external_id, :unique => true
  end
end
